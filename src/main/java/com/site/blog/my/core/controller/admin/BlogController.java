package com.site.blog.my.core.controller.admin;

import com.site.blog.my.core.config.Constants;
import com.site.blog.my.core.entity.Blog;
import com.site.blog.my.core.service.BlogService;
import com.site.blog.my.core.service.CategoryService;
import com.site.blog.my.core.util.*;
import com.site.blog.my.core.util.UUID;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/admin")
public class BlogController {

    @Resource
    private BlogService blogService;
    @Resource
    private CategoryService categoryService;

    @GetMapping("/blogs/list")
    @ResponseBody
    public Result list(@RequestParam Map<String, Object> params) {
        if (StringUtils.isEmpty(params.get("page")) || StringUtils.isEmpty(params.get("limit"))) {
            return ResultGenerator.genFailResult("Tham số sai định dạng");
        }
        PageQueryUtil pageUtil = new PageQueryUtil(params);
        return ResultGenerator.genSuccessResult(blogService.getBlogsPage(pageUtil));
    }


    @GetMapping("/blogs")
    public String list(HttpServletRequest request) {
        request.setAttribute("path", "blogs");
        return "admin/blog";
    }

    @GetMapping("/blogs/edit")
    public String edit(HttpServletRequest request) {
        request.setAttribute("path", "edit");
        request.setAttribute("categories", categoryService.getAllCategories());
        return "admin/edit";
    }

    @GetMapping("/blogs/edit/{blogId}")
    public String edit(HttpServletRequest request, @PathVariable("blogId") Long blogId) {
        request.setAttribute("path", "edit");
        Blog blog = blogService.getBlogById(blogId);
        if (blog == null) {
            return "error/error_400";
        }
        request.setAttribute("blog", blog);
        request.setAttribute("categories", categoryService.getAllCategories());
        return "admin/edit";
    }

    @PostMapping("/blogs/save")
    @ResponseBody
    public Result save(@RequestParam("blogTitle") String blogTitle,
                       @RequestParam(name = "blogSubUrl", required = false) String blogSubUrl,
                       @RequestParam("blogCategoryId") Integer blogCategoryId,
                       @RequestParam("blogTags") String blogTags,
                       @RequestParam("blogContent") String blogContent,
                       @RequestParam("blogCoverImage") String blogCoverImage,
                       @RequestParam("blogStatus") Byte blogStatus,
                       @RequestParam("enableComment") Byte enableComment) {
        if (StringUtils.isEmpty(blogTitle)) {
            return ResultGenerator.genFailResult("Vui lòng nhập tiêu đề của bài viết");
        }
        if (blogTitle.trim().length() > 150) {
            return ResultGenerator.genFailResult("Tiêu đề quá dài");
        }
        if (StringUtils.isEmpty(blogTags)) {
            return ResultGenerator.genFailResult("Vui lòng nhập thẻ bài viết");
        }
        if (blogTags.trim().length() > 150) {
            return ResultGenerator.genFailResult("Tag quá dài");
        }
        if (blogSubUrl.trim().length() > 150) {
            return ResultGenerator.genFailResult("Đường dẫn quá dài");
        }
        if (StringUtils.isEmpty(blogContent)) {
            return ResultGenerator.genFailResult("Vui lòng nhập nội dung bài viết");
        }
        if (blogTags.trim().length() > 100000) {
            return ResultGenerator.genFailResult("Nội dung quá dài");
        }
        if (StringUtils.isEmpty(blogCoverImage)) {
            return ResultGenerator.genFailResult("Ảnh bìa không được để trống");
        }
        Blog blog = new Blog();
        modifyBlog(blogTitle, blogSubUrl, blogCategoryId, blogTags, blogContent, blogCoverImage, blogStatus, enableComment, blog);
        String saveBlogResult = blogService.saveBlog(blog);
        if ("success".equals(saveBlogResult)) {
            return ResultGenerator.genSuccessResult("Thêm thành công");
        } else {
            return ResultGenerator.genFailResult(saveBlogResult);
        }
    }

    @PostMapping("/blogs/update")
    @ResponseBody
    public Result update(@RequestParam("blogId") Long blogId,
                         @RequestParam("blogTitle") String blogTitle,
                         @RequestParam(name = "blogSubUrl", required = false) String blogSubUrl,
                         @RequestParam("blogCategoryId") Integer blogCategoryId,
                         @RequestParam("blogTags") String blogTags,
                         @RequestParam("blogContent") String blogContent,
                         @RequestParam("blogCoverImage") String blogCoverImage,
                         @RequestParam("blogStatus") Byte blogStatus,
                         @RequestParam("enableComment") Byte enableComment) {
        if (StringUtils.isEmpty(blogTitle)) {
            return ResultGenerator.genFailResult("Vui lòng nhập tiêu đề của bài viết");
        }
        if (blogTitle.trim().length() > 150) {
            return ResultGenerator.genFailResult("Tiêu đề quá dài");
        }
        if (StringUtils.isEmpty(blogTags)) {
            return ResultGenerator.genFailResult("Vui lòng nhập tag bài viết");
        }
        if (blogTags.trim().length() > 150) {
            return ResultGenerator.genFailResult("Tag quá dài");
        }
        if (blogSubUrl.trim().length() > 150) {
            return ResultGenerator.genFailResult("Đường dẫn quá dài");
        }
        if (StringUtils.isEmpty(blogContent)) {
            return ResultGenerator.genFailResult("Vui lòng nhập nội dung bài viết");
        }
        if (blogTags.trim().length() > 100000) {
            return ResultGenerator.genFailResult("Nội dung quá dài");
        }
        if (StringUtils.isEmpty(blogCoverImage)) {
            return ResultGenerator.genFailResult("Ảnh bìa không được để trống");
        }
        Blog blog = new Blog();
        blog.setBlogId(blogId);
        modifyBlog(blogTitle, blogSubUrl, blogCategoryId, blogTags, blogContent, blogCoverImage, blogStatus, enableComment, blog);
        String updateBlogResult = blogService.updateBlog(blog);
        if ("success".equals(updateBlogResult)) {
            return ResultGenerator.genSuccessResult("Đã sửa đổi thành công");
        } else {
            return ResultGenerator.genFailResult(updateBlogResult);
        }
    }

    private void modifyBlog(@RequestParam("blogTitle") String blogTitle, @RequestParam(name = "blogSubUrl", required = false) String blogSubUrl, @RequestParam("blogCategoryId") Integer blogCategoryId, @RequestParam("blogTags") String blogTags, @RequestParam("blogContent") String blogContent, @RequestParam("blogCoverImage") String blogCoverImage, @RequestParam("blogStatus") Byte blogStatus, @RequestParam("enableComment") Byte enableComment, Blog blog) {
        blog.setBlogTitle(blogTitle);
        blog.setBlogSubUrl(blogSubUrl);
        blog.setBlogCategoryId(blogCategoryId);
        blog.setBlogTags(blogTags);
        blog.setBlogContent(blogContent);
        blog.setBlogCoverImage(blogCoverImage);
        blog.setBlogStatus(blogStatus);
        blog.setEnableComment(enableComment);
    }

    @PostMapping("/blogs/md/uploadfile")
    public void uploadFileByEditormd(HttpServletRequest request,
                                     HttpServletResponse response,
                                     @RequestParam(name = "editormd-image-file", required = true)
                                             MultipartFile multipartFile) throws IOException {
        try {
            String fileName = UUID.captchaChar(10, true) + ".jpg";
            String fileKey = TaleUtils.getFileKey(fileName);
            File file = new File(Constant.CLASSPATH + fileKey);
            String fileUrl = MyBlogUtils.getHost(new URI(request.getRequestURL() + "")) + fileKey;
            FileCopyUtils.copy(multipartFile.getInputStream(), new FileOutputStream(file));
            request.setCharacterEncoding("utf-8");
            response.setHeader("Content-Type", "text/html");
            response.getWriter().write("{\"success\": 1, \"message\":\"success\",\"url\":\"" + fileUrl + "\"}");
        } catch (IOException | URISyntaxException e) {
            response.getWriter().write("{\"success\":0}");
        }
    }

    @PostMapping("/blogs/delete")
    @ResponseBody
    public Result delete(@RequestBody Integer[] ids) {
        if (ids.length < 1) {
            return ResultGenerator.genFailResult("Tham số sai！");
        }
        if (blogService.deleteBatch(ids)) {
            return ResultGenerator.genSuccessResult();
        } else {
            return ResultGenerator.genFailResult("không xóa được");
        }
    }

}
