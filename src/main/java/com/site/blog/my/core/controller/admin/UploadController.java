package com.site.blog.my.core.controller.admin;

import com.site.blog.my.core.util.*;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@Controller
@RequestMapping("/admin")
public class UploadController {

    @PostMapping({"/upload/file"})
    @ResponseBody
    public Result upload(HttpServletRequest request, @RequestParam("file") MultipartFile multipartFile) throws URISyntaxException {
        try {
            String fileName = UUID.captchaChar(10, true) + ".jpg";
            String fileKey = TaleUtils.getFileKey(fileName);
            File file = new File(Constant.CLASSPATH + fileKey);
            String fileUrl = MyBlogUtils.getHost(new URI(request.getRequestURL() + "")) + fileKey;
            FileCopyUtils.copy(multipartFile.getInputStream(), new FileOutputStream(file));
            Result resultSuccess = ResultGenerator.genSuccessResult();
            resultSuccess.setData(fileUrl);
            return resultSuccess;
        } catch (IOException e) {
            e.printStackTrace();
            return ResultGenerator.genFailResult("Tải lên tệp không thành công");
        }
    }

}
