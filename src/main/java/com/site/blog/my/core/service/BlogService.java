package com.site.blog.my.core.service;

import com.site.blog.my.core.controller.vo.BlogDetailVO;
import com.site.blog.my.core.controller.vo.SimpleBlogListVO;
import com.site.blog.my.core.entity.Blog;
import com.site.blog.my.core.util.PageQueryUtil;
import com.site.blog.my.core.util.PageResult;

import java.util.List;

public interface BlogService {
    String saveBlog(Blog blog);

    PageResult getBlogsPage(PageQueryUtil pageUtil);

    Boolean deleteBatch(Integer[] ids);

    int getTotalBlogs();

    /**
     * Nhận thông tin chi tiết dựa trên id
     *
     * @param blogId
     * @return
     */
    Blog getBlogById(Long blogId);

    /**
     * Sửa đổi nền
     *
     * @param blog
     * @return
     */
    String updateBlog(Blog blog);

    /**
     * Nhận danh sách các bài báo trên trang chủ
     *
     * @param page
     * @return
     */
    PageResult getBlogsForIndexPage(int page);

    /**
     * Danh sách dữ liệu thanh bên của trang chủ
     * 0 lần nhấp chuột nhất-bản phát hành mới nhất
     *
     * @param type
     * @return
     */
    List<SimpleBlogListVO> getBlogListForIndexPage(int type);
    /**
     * Chi tiết bài viết
     *
     * @param blogId
     * @return
     */
    BlogDetailVO getBlogDetail(Long blogId);

    /**
     * Nhận danh sách các bài viết dựa trên các thẻ
     *
     * @param tagName
     * @param page
     * @return
     */
    PageResult getBlogsPageByTag(String tagName, int page);

    /**
     * Nhận danh sách các bài báo theo thể loại
     *
     * @param categoryId
     * @param page
     * @return
     */
    PageResult getBlogsPageByCategory(String categoryId, int page);

    /**
     * Nhận danh sách các bài báo dựa trên tìm kiếm
     *
     * @param keyword
     * @param page
     * @return
     */
    PageResult getBlogsPageBySearch(String keyword, int page);

    BlogDetailVO getBlogDetailBySubUrl(String subUrl);
}
