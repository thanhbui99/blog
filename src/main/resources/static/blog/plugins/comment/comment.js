$('#commentSubmit').click(function () {
    var blogId = $('#blogId').val();
    var verifyCode = $('#verifyCode').val();
    var commentator = $('#commentator').val();
    var email = $('#email').val();
    var websiteUrl = $('#websiteUrl').val();
    var commentBody = $('#commentBody').val();
    if (isNull(blogId)) {
        swal("Tham số sai định dạng", {
            icon: "warning",
        });
        return;
    }
    if (isNull(commentator)) {
        swal("Nội dung cần nhập chưa đủ", {
            icon: "warning",
        });
        return;
    }
    if (isNull(email)) {
        swal("Xin hãy nhập địa chỉ email", {
            icon: "warning",
        });
        return;
    }
    if (isNull(verifyCode)) {
        swal("Xin hãy nhập mã xác minh", {
            icon: "warning",
        });
        return;
    }
    if (!validCN_ENString2_100(commentator)) {
        swal("Vui lòng nhập tên đáp ứng các thông số kỹ thuật (không nhập các ký tự đặc biệt", {
            icon: "warning",
        });
        return;
    }
    if (!validCN_ENString2_100(commentBody)) {
        swal("Vui lòng nhập nội dung bình luận đáp ứng thông số kỹ thuật (không nhập ký tự đặc biệt", {
            icon: "warning",
        });
        return;
    }
    var data = {
        "blogId": blogId, "verifyCode": verifyCode, "commentator": commentator,
        "email": email, "websiteUrl": websiteUrl, "commentBody": commentBody
    };
    console.log(data);
    $.ajax({
        type: 'POST',//Method type
        url: '/blog/comment',
        data: data,
        success: function (result) {
            if (result.resultCode == 200) {
                swal("Bình luận được gửi thành công, vui lòng đợi blogger đánh giá", {
                    icon: "success",
                });
                $('#commentBody').val('');
                $('#verifyCode').val('');
            }
            else {
                swal(result.message, {
                    icon: "error",
                });
            }
            ;
        },
        error: function () {
            swal("operation failed", {
                icon: "error",
            });
        }
    });
});