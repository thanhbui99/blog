/**
 * jqGrid Translation
 * Coffee rabbit yanhonglei@gmail.com
 * http://www.kafeitu.me 
 * 
 * granite marbleqi@163.com
 * 
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html 
**/
/*global jQuery, define */
(function( factory ) {
	"use strict";
	if ( typeof define === "function" && define.amd ) {
		// AMD. Register as an anonymous module.
		define([
			"jquery",
			"../grid.base"
		], factory );
	} else {
		// Browser globals
		factory( jQuery );
	}
}(function( $ ) {

$.jgrid = $.jgrid || {};
if(!$.jgrid.hasOwnProperty("regional")) {
	$.jgrid.regional = [];
}
$.jgrid.regional["en"] = {
    defaults : {
        recordtext: "Article {0} to Article {1}\u3000 Total {2}", // There is a full-width space before the co-character
        emptyrecords: "No record！",
        loadtext: "Loading...",
	savetext: "saving...",
        pgtext : "Page {0}\u3000 A total of {1} pages",
		pgfirst : "First page",
		pglast : "the last page",
		pgnext : "Next page",
		pgprev : "Previous page",
		pgrecs : "Number of records per page",
		showhide: "Toggle expand collapse table",
		// mobile
		pagerCaption : "Table::Page Setup",
		pageText : "Page:",
		recordPage : "Number of records per page",
		nomorerecs : "No more records...",
		scrollPullup: "load more...",
		scrollPulldown : "Refresh...",
		scrollRefresh : "Scroll refresh..."
    },
    search : {
        caption: "search...",
        Find: "Find",
        Reset: "Reset",
        odata: [{ oper:'eq', text:'equal\u3000\u3000'},{ oper:'ne', text:'not equal to\u3000'},{ oper:'lt', text:'Less than\u3000\u3000'},{ oper:'le', text:'Less than or equal to'},{ oper:'gt', text:'more than the\u3000\u3000'},{ oper:'ge', text:'greater or equal to'},{ oper:'bw', text:'Starts with'},{ oper:'bn', text:'Not at the beginning'},{ oper:'in', text:'belong\u3000\u3000'},{ oper:'ni', text:'Does not belong'},{ oper:'ew', text:'The end is'},{ oper:'en', text:'Not at the end'},{ oper:'cn', text:'Include\u3000\u3000'},{ oper:'nc', text:'Does not contain'},{ oper:'nu', text:'Is empty'},{ oper:'nn', text:'not null'}, {oper:'bt', text:'Interval'}],
        groupOps: [ { op: "AND", text: "Meet all conditions" },    { op: "OR",  text: "Meet any condition" } ],
		operandTitle : "Click to search。",
		resetTitle : "Reset search criteria",
		addsubgrup : "Add condition group",
		addrule : "Add condition",
		delgroup : "Delete condition group",
		delrule : "Delete condition"
    },
    edit : {
        addCaption: "Add record",
        editCaption: "Edit record",
        bSubmit: "submit",
        bCancel: "Cancel",
        bClose: "closure",
        saveData: "Data has been modified，Whether to save？",
        bYes : "Yes",
        bNo : "no",
        bExit : "Cancel",
        msg: {
            required:"This field is required",
            number:"Please enter a valid number",
            minValue:"The input value must be greater than or equal to ",
            maxValue:"The input value must be less than or equal to ",
            email: "This is not valid-mail address",
            integer: "Please enter a valid integer",
            date: "Please enter a valid time",
            url: "Invalid URL。The prefix must be ('http://' or 'https://')",
            nodefined : " Undefined！",
            novalue : " Need return value！",
            customarray : "Custom function needs to return an array！",
            customfcheck : "Must have a custom function!"
        }
    },
    view : {
        caption: "View records",
        bClose: "closure"
    },
    del : {
        caption: "delete",
        msg: "Delete selected records？",
        bSubmit: "delete",
        bCancel: "Cancel"
    },
    nav : {
        edittext: "",
        edittitle: "Edit selected record",
        addtext:"",
        addtitle: "Add new record",
        deltext: "",
        deltitle: "Delete selected records",
        searchtext: "",
        searchtitle: "Find",
        refreshtext: "",
        refreshtitle: "Refresh table",
        alertcap: "Notice",
        alerttext: "Please select a record",
        viewtext: "",
        viewtitle: "View selected records",
		savetext: "",
		savetitle: "Keep records",
		canceltext: "",
		canceltitle : "Cancel edit record",
		selectcaption : "operate..."
    },
    col : {
        caption: "Select column",
        bSubmit: "Sure",
        bCancel: "Cancel"
    },
    errors : {
        errcap : "Error",
        nourl : "No url is set",
        norecords: "There are no records to process",
        model : "colNames and colModel have different lengths！"
    },
    formatter : {
        integer : {thousandsSeparator: ",", defaultValue: '0'},
        number : {decimalSeparator:".", thousandsSeparator: ",", decimalPlaces: 2, defaultValue: '0.00'},
        currency : {decimalSeparator:".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "", suffix:"", defaultValue: '0.00'},
        date : {
            dayNames:   [
                "day", "one", "two", "three", "Four", "five", "six",
                "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday",
            ],
            monthNames: [
                "one", "two", "three", "Four", "five", "six", "seven", "Eight", "Nine", "ten", "eleven", "twelve",
                "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
            ],
            AmPm : ["am","pm","morning","afternoon"],
            S: function (j) {return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th';},
            srcformat: 'Y-m-d',
            newformat: 'Y-m-d',
            parseRe : /[#%\\\/:_;.,\t\s-]/,
            masks : {
                // see http://php.net/manual/en/function.date.php for PHP format used in jqGrid
                // and see http://docs.jquery.com/UI/Datepicker/formatDate
                // and https://github.com/jquery/globalize#dates for alternative formats used frequently
                // one can find on https://github.com/jquery/globalize/tree/master/lib/cultures many
                // information about date, time, numbers and currency formats used in different countries
                // one should just convert the information in PHP format
                ISO8601Long:"Y-m-d H:i:s",
                ISO8601Short:"Y-m-d",
                // short date:
                //    n - Numeric representation of a month, without leading zeros
                //    j - Day of the month without leading zeros
                //    Y - A full numeric representation of a year, 4 digits
                // example: 3/1/2012 which means 1 March 2012
                ShortDate: "n/j/Y", // in jQuery UI Datepicker: "M/d/yyyy"
                // long date:
                //    l - A full textual representation of the day of the week
                //    F - A full textual representation of a month
                //    d - Day of the month, 2 digits with leading zeros
                //    Y - A full numeric representation of a year, 4 digits
                LongDate: "l, F d, Y", // in jQuery UI Datepicker: "dddd, MMMM dd, yyyy"
                // long date with long time:
                //    l - A full textual representation of the day of the week
                //    F - A full textual representation of a month
                //    d - Day of the month, 2 digits with leading zeros
                //    Y - A full numeric representation of a year, 4 digits
                //    g - 12-hour format of an hour without leading zeros
                //    i - Minutes with leading zeros
                //    s - Seconds, with leading zeros
                //    A - Uppercase Ante meridiem and Post meridiem (AM or PM)
                FullDateTime: "l, F d, Y g:i:s A", // in jQuery UI Datepicker: "dddd, MMMM dd, yyyy h:mm:ss tt"
                // month day:
                //    F - A full textual representation of a month
                //    d - Day of the month, 2 digits with leading zeros
                MonthDay: "F d", // in jQuery UI Datepicker: "MMMM dd"
                // short time (without seconds)
                //    g - 12-hour format of an hour without leading zeros
                //    i - Minutes with leading zeros
                //    A - Uppercase Ante meridiem and Post meridiem (AM or PM)
                ShortTime: "g:i A", // in jQuery UI Datepicker: "h:mm tt"
                // long time (with seconds)
                //    g - 12-hour format of an hour without leading zeros
                //    i - Minutes with leading zeros
                //    s - Seconds, with leading zeros
                //    A - Uppercase Ante meridiem and Post meridiem (AM or PM)
                LongTime: "g:i:s A", // in jQuery UI Datepicker: "h:mm:ss tt"
                SortableDateTime: "Y-m-d\\TH:i:s",
                UniversalSortableDateTime: "Y-m-d H:i:sO",
                // month with year
                //    Y - A full numeric representation of a year, 4 digits
                //    F - A full textual representation of a month
                YearMonth: "F, Y" // in jQuery UI Datepicker: "MMMM, yyyy"
            },
            reformatAfterEdit : false,
			userLocalTime : false
        },
        baseLinkUrl: '',
        showAction: '',
        target: '',
        checkbox : {disabled:true},
        idName : 'id'
    },
	colmenu : {
		sortasc : "Sort ascending",
		sortdesc : "Sort descending",
		columns : "columns",
		filter : "filter",
		grouping : "Classification",
		ungrouping : "Uncategorize",
		searchTitle : "Find:",
		freeze : "freeze",
		unfreeze : "Unfreeze",
		reorder : "rearrange"
	}
};
}));
